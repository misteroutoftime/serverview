defmodule ButterServers.ServerTest do
  use ExUnit.Case
  alias ButterServers.Server.{Dummy}
  alias ButterServers.Server
  doctest ButterServers.Server

  test "the truth" do
    server_count =
      1..10
      |> Enum.map(fn _x ->  %Dummy{name: "test"}end)
    |>Enum.map(&(Server.create &1))
      |> Enum.map(fn {:ok, server} -> Server.start(server) end)
      |> Enum.map(fn {:ok, server} -> Server.status(server) end)
      |> Enum.filter(fn {:ok, status, server} -> status == :running end)
      |> Enum.count()

    assert server_count == 10
  end
end
