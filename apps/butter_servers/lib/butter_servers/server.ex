defprotocol ButterServers.Server do
  @type t :: map()
  @spec create(map()) ::
          {:ok, __MODULE__.t()} | {:error, reason :: String.t()}
  def create(server_initial_data)

  @spec delete(__MODULE__.t()) ::
          {:ok, __MODULE__.t()}
          | {:error, __MODULE__.t(), reason :: String.t()}
          | {:error, reason :: String.t()}

  def delete(server)

  @spec start(__MODULE__.t()) ::
          {:ok, __MODULE__.t()}
          | {:error, __MODULE__.t(), reason :: String.t()}
  def start(server)

  @spec stop(__MODULE__.t()) ::
          {:ok, __MODULE__.t()}
          | {:error, __MODULE__.t(), reason :: String.t()}
  def stop(server)

  @spec status(__MODULE__.t()) ::
          {:ok, status :: :stopped | :running | :created | :starting | :stopping,
           server :: __MODULE__.t()}
          | {:error, reasion :: String.t()}
  def status(server)
end

defmodule ButterServers.Server.Dummy do
  alias ButterServers.Server
  defstruct status: :created, name: "test"

  defimpl Server do
    def create(_config), do: {:ok, %Server.Dummy{status: :created}}

    def delete(%Server.Dummy{status: status}) do
      case status do
        :off -> {:ok}
        smth -> {:error, smth}
      end
    end

    def start(%Server.Dummy{status: status} = server) do
      case status do
        :running -> {:ok, server}
        status when status in [:stopped, :created] -> {:ok, %{server | status: :running}}
        smth -> {:error, smth}
      end
    end

    def stop(%Server.Dummy{status: status} = server) do
      case status do
        :stopped -> {:ok, server}
        :running -> {:ok, %{status: :stopped}}
        smth -> {:error, smth}
      end
    end

    def status(%Server.Dummy{status: status} = server), do: {:ok, status, server}
  end
end
