defmodule ButterServersWeb.Router do
  use ButterServersWeb, :router
  import Phoenix.LiveView.Router
  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :put_root_layout, {ButterServersWeb.LayoutView, :root}
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ButterServersWeb do
    pipe_through :browser
    live "/servers", ServersLiveView
    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", ButterServersWeb do
  #   pipe_through :api
  # end
end
