defmodule ButterServersWeb.ServersLiveView do
  use Phoenix.LiveView, layout: {ButterServersWeb.LayoutView, "live.html"}
  use Phoenix.HTML
  alias ButterServers.Server

  def mount(_params, %{"current_user_id" => user_id}, socket) do
    if connected?(socket), do: :timer.send_interval(500, self(), :update)
    {:ok, server} = Server.create(%Server.Dummy{})

    case Server.status(server) do
      {:ok, status, server} ->
        {:ok, assign(socket, server_status: status, user_id: user_id)}

      {:error, reason} ->
        {:error, reason}
    end
  end

  def mount(params, %{}, socket) do
    mount(params, %{"current_user_id" => 0}, socket)
  end

  def handle_info(:update, socket) do
    {:ok, server} = Server.create(%Server.Dummy{})
    {:ok, status, _server} = Server.status(server)

    {:noreply, assign(socket, :server_status, status)}
  end

  def render(assigns) do
    ~L"""
    Current Status <%=  @server_status %>

    """
  end
end
