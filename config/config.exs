# This file is responsible for configuring your umbrella
# and **all applications** and their dependencies with the
# help of Mix.Config.
#
# Note that all applications in your umbrella share the
# same configuration and dependencies, which is why they
# all use the same configuration file. If you want different
# configurations or dependencies per app, it is best to
# move said applications out of the umbrella.
use Mix.Config



config :butter_servers_web,
  generators: [context_app: :butter_servers]

# Configures the endpoint
config :butter_servers_web, ButterServersWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "F2o3cTs53tt8vy9RFKHCgntmh9ngAGxbI6iUowE+mH4tkCs8GhP5ePapVKxhTD1O",
  render_errors: [view: ButterServersWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: ButterServersWeb.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "UpQpOw87"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason




# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
